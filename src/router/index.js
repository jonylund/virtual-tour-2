import Vue from "vue";
import VueRouter from "vue-router";

// loads views when needed
function load(component) {
  return () => import(`@/views/${component}.vue`);
}

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: load("Home")
  },
  {
    path: "/admin",
    name: "Admin",
    component: load("Admin")
  },
  {
    path: "/1",
    name: "Version1",
    component: load("Version1")
  },
  {
    path: "/2",
    name: "Version2",
    component: load("Version2")
  },
  {
    path: "/3",
    name: "Version3",
    component: load("Version3")
  }
];

const router = new VueRouter({
  routes
});

export default router;
